/// <reference types = "Cypress" />
import {Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";
import ProductDetail from "../Pages/ProductDetailPage";

    
Given(`User visit Luminskin store URL`, function () {
    ProductDetail.homeScreen()
    Cypress.on('uncaught:exception', (err) => {
        return false
                        })
    cy.wait(10000)
});
When(`User clicks on product`,function(){
    ProductDetail.ClickProduct()
})
Then(`User is directed to detail page`,function(){
    ProductDetail.getProductModal()
})
And(`User clicks on add to cart and modal popped up`, function(){
    ProductDetail.ClickaddCart()
})
And(`User clicks to change product image`,function(){
    ProductDetail.ViewImage()
})