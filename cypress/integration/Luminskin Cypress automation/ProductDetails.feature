Feature: ProductDetails

  As a User
  I should be able to view Product detail page

  As a User
  I should be able to add product to cart

  As a User
  I should be able to view product image
 

  Scenario: I should be able to view Product detail page

    Given User visit Luminskin store URL
    When User clicks on product
    Then User is directed to detail page
    
    
    Scenario:I should be able to add product to cart
    Given User visit Luminskin store URL
    When User clicks on product
    Then User is directed to detail page
    And User clicks on add to cart and modal popped up
    
    

    Scenario:I should be able to view product image
     Given User visit Luminskin store URL
    When User clicks on product
    Then User is directed to detail page
    And User clicks to change product image
    
  
    
    