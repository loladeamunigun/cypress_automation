/// <reference types = "Cypress" />
import {Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";
import Checkout from "../Pages/CheckoutPage";

    
Given(`User visit Luminskin store URL`, function () {
    Checkout.homeScreen()
    Cypress.on('uncaught:exception', (err) => {
        return false
                        })
    cy.wait(10000)
});
When(`User clicks on product`,function(){
    Checkout.ClickProduct()
})
Then(`User is directed to detail page`,function(){
    Checkout.getProductModal()
})
And(`User clicks on add to cart and modal popped up`, function(){
    Checkout.ClickaddCart()
})
Then(`User clicks on Checkout`,function(){
    Checkout.ClickCheckout()
})
And(`User click add to cart`,function(){
    Checkout.ClickAddtoCart()
})
When(`User proceed to checkout`,function(){
    Checkout.Proceed()
})