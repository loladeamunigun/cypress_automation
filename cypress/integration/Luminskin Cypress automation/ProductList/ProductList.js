/// <reference types = "Cypress" />
import {Given, When, Then } from "cypress-cucumber-preprocessor/steps";
import ProductList from "../Pages/ProductListPage";

    
Given(`User visit Luminskin store URL`, function () {
    ProductList.homeScreen()
    Cypress.on('uncaught:exception', (err) => {
        return false
                        })
    cy.wait(10000)
});
Then(`User can view the List of products`, function(){
    ProductList.GetProduct()
});
When(`User clicks on filter and selects skincare`,function(){
    ProductList.chooseFilter()
})
When(`User navigates and clicks on add to cart`, function(){
    ProductList.ClickaddCart()
})
Then(`Page displays modal and user clicks add to cart`,function(){
    ProductList.getModal()
})