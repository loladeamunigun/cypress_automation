
const BaseUrl = 'https://store.luminskin.com/products'
const Product = ':nth-child(3) > .chakra-stack > .css-1bqnmih > .chakra-link > .css-himp6n > .chakra-image'
const ProductMod='.css-1vslpuw'
const addCart='.chakra-button'
const arrow='.css-zti011 > .chakra-icon'

class ProductDetail {
    
    static homeScreen () {
        cy.visit(BaseUrl)    
    }
    static ClickProduct() {
        cy.get(Product).click()
    }
    static getProductModal(){
        cy.get(ProductMod).contains('Classic Maintenance Set')
    }
    static ClickaddCart(){
        cy.get(addCart).click()
    }
    static ViewImage(){
        cy.get(arrow).click()
    }
}
export default ProductDetail