
const BaseUrl = 'https://www.luminskin.com/'
const LuminLogo = '.css-nls69x'
const Account = '.css-25ttez'
const email='.chakra-stack > .chakra-input'
const clickEmail='.chakra-stack > .chakra-button'
const errorModal='.css-1jvdl73'
const Lang='.chakra-select'
const clickOk='.css-ngj2ua > .chakra-stack > .chakra-button'
const cart='.css-1x5dvzm'
const cartmod='#cart-form'
const currency='.currency-select'


class Landing {
    
    static homeScreen () {
        cy.visit(BaseUrl)    
    }
    static GetLogo() {
        cy.get(LuminLogo)
    }
    static ClickAccount() {
        cy.get(Account).click()
    }
    static EnterInvalidEmail(){
        cy.get(email).type('amunigunololade@ymail.com')
        cy.wait(1000)
        cy.get(clickEmail).click()
    }
    static errormessage(){
        cy.get(errorModal)
        cy.wait(1000)
        cy.get(clickOk).click()
    }
    static ClickLanguage(){
        cy.get(Lang).select('FR')
    }
    static clickCart(){
        cy.get(cart).click()
    }
   static CartModal(){
       cy.get(cartmod)
   }
   static CurrencyButton(){
       cy.get(currency).select('USD')
   }
}
export default Landing