
const BaseUrl = 'https://store.luminskin.com/products'
const Product = ':nth-child(3) > .chakra-stack > .css-1bqnmih > .chakra-link > .css-himp6n > .chakra-image'
const ProductMod='.css-1vslpuw'
const addCart='.chakra-button'
const Chkout='.open'
const ATC='.checkout-btn'
const clickProceed='#cart-form > .cart-footer2 > .react-cart-buttons > .checkout-btn'
const modal='.css-ay34j9'


class Checkout {
    
    static homeScreen () {
        cy.visit(BaseUrl)    
    }
    static ClickProduct() {
        cy.get(Product).click()
    }
    static getProductModal(){
        cy.get(ProductMod).contains('Classic Maintenance Set')
    }
    static ClickaddCart(){
        cy.get(addCart).click()
    }
    static ClickCheckout(){
        cy.get(Chkout).click()
    }
    static ClickAddtoCart(){
        cy.get(ATC).click()
    }
    static Proceed(){
        cy.get(clickProceed).click()
        cy.wait(1000)
        cy.get(modal).contains('Shipping Address')
    }
}
export default Checkout