
const BaseUrl = 'https://store.luminskin.com/products'
const allProduct = '.css-mafqmj > .chakra-heading'
const FilterBy='.css-gjxi74 > .chakra-select__wrapper > .chakra-select'
const addCart=':nth-child(3) > .chakra-stack > .chakra-button'
const modal='.open'
const clickmodalcart='.checkout-btn'

class ProductList {
    
    static homeScreen () {
        cy.visit(BaseUrl)    
    }
    static GetProduct() {
        cy.get(allProduct).contains('All Products')
    }
    static chooseFilter(){
        cy.get(FilterBy).select('Skincare')
    }
    static ClickaddCart(){
        cy.get(allProduct).scrollIntoView()
        cy.get(addCart).click()
    }
    static getModal(){
        cy.get(modal)
        cy.get(clickmodalcart).click()
    }
}
export default ProductList