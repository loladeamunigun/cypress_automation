Feature: ProductList

  As a User
  I should be able to view Product list page

  As a User
  I should be able to filter search

  As a User
  I should be able to add product to cart
 

  Scenario: I should be able to view Product list page

    Given User visit Luminskin store URL
    Then User can view the List of products
    
    
    Scenario:I should be able to filter search
    Given User visit Luminskin store URL
    Then User can view the List of products
    When User clicks on filter and selects skincare
   
    

    Scenario:I should be able to add product to cart
    Given User visit Luminskin store URL
    Then User can view the List of products
    When User navigates and clicks on add to cart
    Then Page displays modal and user clicks add to cart
    
  
    
    