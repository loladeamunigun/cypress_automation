Feature: Landing

  As a User
  I should be able to view Luminskin landing page

  As a User
  I should not be able to Get authentication link with invalid email

  As a User
  I should be able to change language
 
  As a user 
  I should be able to view cart

  As a User
  I should be able to change currency

  Scenario: User should be able to view Luminskin landing page

    Given User visit Luminskin URL
    Then User can view the Lumin Logo
    
    
    Scenario:I should not be able to Get authentication link with invalid email
    Given User visit Luminskin URL
    When User click account
    Then User enter unregistered email
    And Error modal should pop up
    

    Scenario:I should be able to change language
    Given User visit Luminskin URL
    Then User clicks on Language icon and select French
    
  
   Scenario:I should be able to view cart
    Given User visit Luminskin URL
    When User clicks on cart button
    Then Cart page is displayed 

    Scenario:I should be able to change currency
    Given User visit Luminskin URL
    When User clicks on cart button
    Then User select USD from dropdown
  
    
    