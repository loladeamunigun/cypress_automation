/// <reference types = "Cypress" />
import {Given, When, Then } from "cypress-cucumber-preprocessor/steps";
import Landing from "../Pages/LandingPage";

    
Given(`User visit Luminskin URL`, function () {
    Landing.homeScreen()
    Cypress.on('uncaught:exception', (err) => {
        return false
                        })
    cy.wait(10000)
});
Then(`User can view the Lumin Logo`, function(){
    Landing.GetLogo()
});
When(`User click account`, function() {
    Landing.ClickAccount()
})
Then(`User enter unregistered email`,function(){
    Landing.EnterInvalidEmail()
})
And(`Error modal should pop up`,function(){
    Landing.errormessage()
})
Then(`User clicks on Language icon and select French`,function(){
    Landing.ClickLanguage()
})
When(`User clicks on cart button`,function(){
    Landing.clickCart()
})
Then(`Cart page is displayed`,function(){
    Landing.CartModal()
})
Then(`User select USD from dropdown`,function(){
    Landing.CurrencyButton()
})